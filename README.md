Common:

1. Create Maven project

2. Use TestNG & Selenium to develop tests

3. One task == one test method

4. Use xml as data source for data

5. Use property file(s) for properties


Task 1

1. Open gmail & login

2. Click "Compose" button

3. Fill "To", "Subject" & "message" fields

4. Click "send" button

5. Verify that message is in "sent" folder

6. Remove message from the "sent" folder


To run the selenium_hw_3, you need:

 - run this command: "mvn clean test" (you will use default webDriver - chrome);

 - if not chrome webDriver will be use,  in order to change webDriver you have to add

property "-Dbrowser=firefox". At the moment chrome and firefox are supported.;


To get a generated report for selenium_hw_3, you need:

 - run this command: "mvn allure:serve".