package ua.com.epam.tests;

import io.qameta.allure.Epic;
import org.testng.annotations.Test;
import ua.com.epam.businessobject.GmailHomeBO;
import ua.com.epam.businessobject.GmailLoginBO;
import ua.com.epam.businessobject.SentMessageBO;

import static org.testng.Assert.assertTrue;

public class GmailMessageTest extends BaseTest {

    @Epic("TESTING - 'https://mail.google.com/'")
    @Test(dataProvider = "userData")
    public void sendGmailMsgAndRemove(String email, String password) {

        GmailLoginBO loginPage = new GmailLoginBO();

        String randomMsg = getRandomNumber();

        GmailHomeBO homePage = loginPage
                .loginToGmail(email, password);

        homePage.writeAndSendMessage(
                dataProp.getRecipient(),
                dataProp.getTopic(),
                randomMsg);

        SentMessageBO sentMessage = homePage.navigateToSentLetter();

        assertTrue(sentMessage.getMessageText().contains(randomMsg)
                , String.format("Letter with message - '%s' wasn't sent.", randomMsg));

        sentMessage.deleteFirstLetter();
    }
}