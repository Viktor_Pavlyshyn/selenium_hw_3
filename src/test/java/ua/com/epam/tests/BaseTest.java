package ua.com.epam.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import ua.com.epam.listener.AllureListener;
import ua.com.epam.utils.DataPropGmail;
import ua.com.epam.utils.DataReader;

import java.util.Random;

import static ua.com.epam.webdriver.LocalDriverManager.closeDriver;
import static ua.com.epam.webdriver.LocalDriverManager.getWebDriver;

@Listeners(AllureListener.class)
public class BaseTest {
    protected DataReader propReader = new DataReader();
    protected DataPropGmail dataProp;

    @BeforeMethod(alwaysRun = true)
    protected void setUp() {
        dataProp = new DataPropGmail();
        getWebDriver().get(dataProp.getGmailPage());
    }

    @DataProvider(name = "userData", parallel = true)
    protected Object[][] users() {
        Object[][] users = propReader.getUsers().stream()
                .map(u -> new Object[]{u.getLogin(), u.getPassword()})
                .toArray(Object[][]::new);
        return users;
    }

    @AfterMethod(alwaysRun = true)
    protected void closeSite() {
        closeDriver();
    }

    protected String getRandomNumber() {
        Random rd = new Random();
        return String.valueOf(rd.nextInt((9999999 - 1) + 1) + 1);
    }
}
