package ua.com.epam.elementdecorator;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ua.com.epam.webdriver.LocalDriverManager;

public class TabElement extends AbstractElement {

    public TabElement(WebElement element) {
        super(element);
    }

    public String getTextAfterStalenessOfElement(String xPathValue) {
        fwait.until(ExpectedConditions.stalenessOf(
                LocalDriverManager.getWebDriver().findElement(By.xpath(xPathValue))));
        return element.getText();
    }

    public TabElement clickAfterVisibilityOf() {
        fwait.until(ExpectedConditions.visibilityOf(element));
        element.click();
        return this;
    }

    public TabElement clickAfterElementToBeClickable() {
        fwait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        return this;
    }

    public TabElement clickAfterDocumentReadyState() {
        fwait.until((ExpectedCondition<Boolean>) wd -> ((JavascriptExecutor) wd)
                .executeScript("return document.readyState").equals("complete"));
        element.click();
        return this;
    }

    public TabElement clickAfterStalenessOfElement(String xpath) {
        fwait.until(ExpectedConditions.stalenessOf(
                LocalDriverManager.getWebDriver().findElement(By.xpath(xpath))));
        element.click();
        return this;
    }
}
