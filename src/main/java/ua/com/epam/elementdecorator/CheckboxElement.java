package ua.com.epam.elementdecorator;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CheckboxElement extends AbstractElement {

    public CheckboxElement(WebElement element) {
        super(element);
    }

    public void clickIfNotSelected() {
        if (!element.isSelected()) {
            element.click();
        }
    }

    public CheckboxElement clickAfterVisibilityOf() {
        fwait.until(ExpectedConditions.visibilityOf(element));
        element.click();
        return this;
    }

    public CheckboxElement clickAfterElementToBeClickable() {
        fwait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        return this;
    }
}
