package ua.com.epam.businessobject;

import lombok.extern.log4j.Log4j2;
import ua.com.epam.pageobject.SentMessagePage;

@Log4j2
public class SentMessageBO {
    private final SentMessagePage sentMessagePage;

    public SentMessageBO() {
        this.sentMessagePage = new SentMessagePage();
    }

    public String getMessageText() {
        log.info("Getting message text.");

        return sentMessagePage.getSentMessage()
                .getTextAfterStalenessOfElement(sentMessagePage.getFirstMessage());
    }

    public SentMessageBO deleteFirstLetter() {
        log.info("Selecting all lettere.");
        sentMessagePage.getAllCheckbox().clickIfNotSelected();

        log.info("Delete a letter.");
        sentMessagePage.getDeleteMsg().clickAfterElementToBeClickable();
        sentMessagePage.getRefSendInClearAreaMsg().getTextAfterDocumentReadyState();
        return this;
    }
}
