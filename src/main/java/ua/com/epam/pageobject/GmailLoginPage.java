package ua.com.epam.pageobject;

import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import ua.com.epam.elementdecorator.ButtonElement;
import ua.com.epam.elementdecorator.InputElement;

@Getter
public class GmailLoginPage extends AbstractPage {
    @FindBy(xpath = "//input[@id='identifierId']")
    private InputElement setLogin;
    @FindBy(xpath = "//button[@jsname='LgbsSe']")
    private ButtonElement submitButtonLogin;
    @FindBy(xpath = "//input[@name='password']")
    private InputElement setPassword;
    @FindBy(xpath = "//button[@jscontroller='soHxf'][ancestor::div[@class='qhFLie']]")
    private ButtonElement submitButtonPassword;

    public GmailLoginPage() {
        super();
    }
}
