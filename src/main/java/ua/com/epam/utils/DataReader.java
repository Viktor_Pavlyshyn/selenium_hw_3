package ua.com.epam.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.config.DataPath;
import ua.com.epam.exception.EmptyFileException;
import ua.com.epam.model.User;
import ua.com.epam.utils.helper.LocalDateAdapter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
public class DataReader implements DataPath {
    private Gson g = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateAdapter()).create();

    public List<User> getUsers() {
        log.info("Try to find users.");

        List<User> users = new ArrayList<>();

        try (Stream<String> lines = Files.lines(Paths.get(GMAIL_USERS))) {
            users = lines.map(s -> g.fromJson(s, User.class)).collect(Collectors.toList());
            if (users.isEmpty()) {
                log.error("File by path - '{}' is empty!", GMAIL_USERS);
                throw new EmptyFileException("File is empty!");
            }
        } catch (IOException e) {
            log.error(String.format("Can't read file by path - %s.", GMAIL_USERS), e);
        }

        log.info(users.size() + " users found!");
        return users;
    }
}
